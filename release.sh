rm -Rf dist
mkdir -p dist/node_modules/matrix-widget-api/dist/
mkdir -p dist/src
cp node_modules/matrix-widget-api/dist/api.js dist/node_modules/matrix-widget-api/dist/
cp src/* dist/src/
cp *.html dist/
ipfs add -r dist/
