let widgetApi = null;
let userId = null;

function handleError(error) {
    console.error(error);
}

function parseFragment() {
    const fragmentString = (window.location.hash || "?");
    return new URLSearchParams(fragmentString.substring(Math.max(fragmentString.indexOf('?'), 0)));
}

function assertParam(fragment, name) {
    const val = fragment.get(name);
    if (!val) throw new Error(`${name} is not present in URL - cannot load widget`);
    return val;
}

async function sendSticker(text, url, size) {
    await widgetApi.sendRoomEvent('m.sticker', {
        url,
        info: {
            h: 200,
            mimetype: 'image/png',
            size,
            thumbnail_info: {
                h: 200,
                mimetype: 'image/png',
                size,
                w: 200
            },
            thumbnail_url: url,
            w: 200
        },
        body: text,
    });
}

window.sendHonk = () => {
    sendSticker('Happy Canada Goose honking', 'mxc://matrix.org/DSAPLLMtVmVMOseXQlabpCrm', 76342);
};

window.sendAngry = () => {
    sendSticker('Angry Canada Goose hissing', 'mxc://matrix.org/eTeLHYeiIoPhyBkcoQuqbsjJ', 102888);
};

window.sendSleeping = () => {
    sendSticker('Sleeping Canada Goose with the text GN8', 'mxc://matrix.org/IsCyREbbitOxNKCafpYLGujV', 100316);
};

try {
    const qs = parseFragment();
    const widgetId = assertParam(qs, 'widgetId');
    userId = assertParam(qs, 'userId');
    console.error(widgetId, userId);

    // Set up the widget API as soon as possible to avoid problems with the client
    widgetApi = new mxwidgets.WidgetApi(widgetId);

    // We want to be able to send game state
    widgetApi.requestCapabilityToSendEvent('m.sticker');

    widgetApi.on('ready', function () {
        console.debug('ready');
    });
    // Start the widget as soon as possible too, otherwise the client might time us out.
    widgetApi.start();
} catch (e) {
    handleError(e);
}
